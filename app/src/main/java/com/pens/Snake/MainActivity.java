package com.pens.Snake;

import android.app.Activity;
import android.app.AppComponentFactory;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Point;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.games.Games;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;

public class MainActivity extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener, View.OnClickListener {

    private static final String TAG = "MainActivity";
    ImageButton play, credit, achiev;

    private GoogleSignInClient mGoogleSignInClient = null;

    final public int BUTTON_SIGN_IN = 2;
    final public int BUTTON_SIGN_OUT = 1;

    final static int RC_SIGN_IN = 9001;

    private static final int RC_LEADERBOARD_UI = 9004;
    private static final int RC_ACHIEVEMENT_UI = 9003;

    Button signOutButton;
    SignInButton signInButton;

    private SharedPreferences load;
    private SharedPreferences save;
    private SharedPreferences.Editor saveEditor;
    private boolean isAuthenticated;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        save = PreferenceManager.getDefaultSharedPreferences(this);
        saveEditor = save.edit();

        load = PreferenceManager.getDefaultSharedPreferences(this);
        isAuthenticated = load.getBoolean("Authenticated", true);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        play = findViewById(R.id.play);
        credit = findViewById(R.id.credit);
        achiev = findViewById(R.id.achiev);
        signInButton = findViewById(R.id.signin);
        signOutButton = findViewById(R.id.signout);

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_GAMES_SIGN_IN)
                .requestEmail()
                .build();

        signOutButton.setId(BUTTON_SIGN_OUT);
        signOutButton.setOnClickListener(this);

        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);
        signInButton.setId(BUTTON_SIGN_IN);
        signInButton.setOnClickListener(this);

        updateUI(null);

        Games.getLeaderboardsClient(this, GoogleSignIn.getLastSignedInAccount(this))
                .submitScore(getString(R.string.leaderboard), 1337);

        play.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainMenu(v);
            }
        });
        achiev.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainMenu(v);
            }
        });
    }

    private void MainMenu(View view){
        Intent intent = null;
        switch (view.getId()){
            case R.id.play:
                intent = new Intent(MainActivity.this, SnakeActivity.class);
                startActivity(intent);
                break;
            case R.id.achiev:
                showLead();
                break;
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case BUTTON_SIGN_IN:
                Intent signInIntent = mGoogleSignInClient.getSignInIntent();
                startActivity(signInIntent);
                isAuthenticated = true;
                updateUI(null);
                break;
            case BUTTON_SIGN_OUT:
                mGoogleSignInClient.revokeAccess().addOnCompleteListener(this,
                        new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {

                            }
                        });
                isAuthenticated = false;
                updateUI(null);
                Toast.makeText(this, "Log out", Toast.LENGTH_SHORT).show();
                break;
            default:
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInClient.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            // The Task returned from this call is always completed, no need to attach
            // a listener.
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            handleSignInResult(task);
        }
    }

    private void handleSignInResult(Task<GoogleSignInAccount> completedTask) {
        try {
            GoogleSignInAccount account = completedTask.getResult(ApiException.class);

            // Signed in successfully, show authenticated UI.
            updateUI(account);
        } catch (ApiException e) {
            // The ApiException status code indicates the detailed failure reason.
            // Please refer to the GoogleSignInStatusCodes class reference for more information.
            Log.w(TAG, "signInResult:failed code=" + e.getStatusCode());
        }
        updateUI(null);
    }

    private void updateUI(@Nullable GoogleSignInAccount account) {
        if (account != null) {
//            signInButton.setVisibility(View.GONE);
//            signOutButton.setVisibility(View.VISIBLE);
        } else {
//            signOutButton.setVisibility(View.GONE);
//            signInButton.setVisibility(View.VISIBLE);
        }

        if (isAuthenticated) {
            signInButton.setVisibility(View.GONE);
            signOutButton.setVisibility(View.VISIBLE);
        }
        else {
            signOutButton.setVisibility(View.GONE);
            signInButton.setVisibility(View.VISIBLE);
        }

        saveEditor.putBoolean("Authenticated", isAuthenticated);
        saveEditor.commit();
    }

    private void showLead(){
        Games.getLeaderboardsClient(this, (GoogleSignIn.getLastSignedInAccount(this)))
                .getLeaderboardIntent(getString(R.string.leaderboard))
                .addOnSuccessListener(new OnSuccessListener<Intent>() {
                    @Override
                    public void onSuccess(Intent intent) {
                        startActivityForResult(intent, RC_LEADERBOARD_UI);
                    }
                });
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }
}
